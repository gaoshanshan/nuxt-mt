import Vue from 'vue'
import Vuex, { Store } from 'vuex'

import geo from './module/geo'
import home from './module/home'

import * as mutationTypes from './mutationTypes'

Vue.use(Vuex)

const store = () =>
  new Store({
    modules: {
      geo,
      home
    },
    actions: {
      async nuxtServerInit({ commit, dispatch }) {
        await Promise.all([
          dispatch(`geo/${mutationTypes.SET_POSITION}`),
          dispatch(`home/${mutationTypes.SET_MENU}`)
        ])
        await dispatch(`home/${mutationTypes.SET_HOT_PLACE}`)
      }
    }
  })

export default store
