import { SET_MENU, SET_HOT_PLACE } from '../mutationTypes'
import axios from '../../server/util/axios'

const state = () => ({ menuList: [], hotPlace: [] })

const mutations = {
  [SET_MENU](state, menu) {
    state.menuList = menu
  },
  [SET_HOT_PLACE](state, hotPlace) {
    state.hotPlace = hotPlace
  }
}

const actions = {
  async [SET_MENU]({ commit }) {
    const {
      status,
      data: { menu }
    } = await axios.get('/geo/menu')
    commit(SET_MENU, status == 200 ? menu : [])
  },
  async [SET_HOT_PLACE]({ commit, rootState }) {
    const city = rootState.geo.position.city
    const {
      status,
      data: { hotPlace }
    } = await axios.get('/search/hotPlace', {
      params: { city }
    })
    commit(SET_HOT_PLACE, status === 200 ? hotPlace : [])
  }
}

export default { namespaced: true, state, mutations, actions }
