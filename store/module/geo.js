import { SET_POSITION } from '../mutationTypes'
import axios from '../../server/util/axios'

const state = () => ({ position: {} })

const mutations = {
  [SET_POSITION](state, val) {
    state.position = val
  }
}

const actions = {
  async [SET_POSITION]({ commit }) {
    const {
      status,
      data: { provinces, city }
    } = await axios.get('/geo/getPosition')
    commit(
      SET_POSITION,
      status == 200 ? { provinces, city } : { provinces: '', city: '' }
    )
  }
}

export default { namespaced: true, state, mutations, actions }
