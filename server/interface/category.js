import Router from 'koa-router'

import Category from '../db/models/Category'

let router = new Router({ prefix: '/category' })

router.get('/crumbs', async ctx => {
  const city = ctx.query.city.replace('市', '') || '北京'
  const result = await Category.findOne({
    city: city
  })
  if (result) {
    ctx.body = {
      areas: result.areas.slice(0, 5),
      types: result.types.slice(0, 5)
    }
  } else {
    ctx.body = {
      areas: [],
      types: []
    }
  }
})

export default router
