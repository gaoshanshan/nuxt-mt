import { Schema, model } from 'mongoose'

const CategorySchema = new Schema({
  city: {
    type: String
  },
  types: {
    type: Array,
    require: true
  },
  areas: {
    type: Array,
    require: true
  }
})

export default model('Category', CategorySchema, 'category')
