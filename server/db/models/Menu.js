import { Schema, model } from 'mongoose'

const menuSchema = new Schema({
  menu: {
    type: Array,
    require: true
  }
})

export default model('Menu', menuSchema)
