import { Schema, model } from 'mongoose'

const citySchema = new Schema({
  id: {
    type: String,
    require: true
  },
  value: {
    type: Array,
    require: true
  }
})

export default model('City', citySchema)
