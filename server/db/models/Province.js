import { Schema, model } from 'mongoose'

const provinceSchema = new Schema({
  id: {
    type: String,
    require: true
  },
  value: {
    type: String,
    require: true
  }
})

export default model('Province', provinceSchema)
