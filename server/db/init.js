import mongoose from 'mongoose'
import Redis from 'koa-redis'

import conf from '../../config/server'

export function connectMongodb() {
  mongoose.connect(
    conf.mongo.url,
    { useNewUrlParser: true }
  )
  mongoose.connection.on('open', () => {
    console.log(`${conf.mongo.url} 连接成功`)
  })
  mongoose.connection.on('error', err => {
    console.log(`连接mongodb错误 ${err}`)
  })
}

let redisConnect = null

export function getRedisConnect() {
  if (redisConnect) return redisConnect
  return (redisConnect = new Redis({
    host: conf.redis.host
  }))
}
