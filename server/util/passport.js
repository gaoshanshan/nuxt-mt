import passport from 'koa-passport'
import LocalStrategy from 'passport-local'

import User from '../db/models/User'

passport.use(new LocalStrategy(async (username, password, done) => {
  const where = {
    username
  }
  const user = await User.findOne(where)
  if (!user) return done(null, false, '用户不存在')
  if (user.password !== password) return (null, false, '用户名或密码错误')
  done(null, user)
}))

passport.serializeUser((user, done) => {
  done(null, user)
})

passport.deserializeUser((user, done) => {
  done(null, user)
})

export default passport
