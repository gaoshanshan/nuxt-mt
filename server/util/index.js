export default {
  getCode() {
    return Math.random().toString(16).slice(2, 6).toUpperCase()
  },
  getExpire() {
    return new Date().getTime() + 1000 * 60 * 3
  }
}
