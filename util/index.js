import CryptoJS from 'crypto-js'

export function MD5(value) {
  return CryptoJS.MD5(value).toString()
}
